package de.sts_plugins.plugins.example;

import de.sts_plugins.utils.FileUtils;
import de.sts_plugins.utils.PluginUtils;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Properties;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import js.java.stspluginlib.PluginClient;

public class Main extends PluginClient {
    
    public Main(String name, String author, String version, String text) {
        super(name, author, version, text);
    }

    public static void main(String[] args) {
        String version = "not set!";
        UUID uuid = null;
        Properties props = new Properties();

        try {
            props.load(Main.class.getResourceAsStream("/de/sts_plugins/plugins/example/Main.properties"));
            version = props.getProperty("version", null);
            uuid = UUID.fromString(props.getProperty("uuid", null));
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("This is: " + uuid + " with Version: " + version);
        System.out.println("Data   Path: " + PluginUtils.getPluginDataPath(uuid));
        System.out.println("Config Path: " + PluginUtils.getPluginConfigPath(uuid));
        FileUtils.ensureDirectory(PluginUtils.getPluginDataPath(uuid));
        FileUtils.ensureDirectory(PluginUtils.getPluginConfigPath(uuid));
        
        System.out.println("Starting plugin");
        Main m = new Main("STSExamplePlugin", "BR 89", version, "Example");
        try {
            m.connect("localhost");
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void connected() {
        request_anlageninfo();
    }

    @Override
    protected void closed() {
    }

    @Override
    protected void response_anlageninfo(int i, String string, String string1) {
        System.out.println("Connected to AID: " + i);
        System.out.println(string + " | " + string1);
        close();
    }

    @Override
    protected void response_bahnsteigliste(HashMap<String, HashSet<String>> hm) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void response_zugliste(HashMap<Integer, String> hm) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void response_zugdetails(int i, ZugDetails zd) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void response_zugfahrplan(int i, LinkedList<ZugFahrplanZeile> ll) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
